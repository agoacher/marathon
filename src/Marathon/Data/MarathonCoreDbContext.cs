﻿namespace Marathon.Data
{
    using Marathon.Data.Model;
    using Microsoft.EntityFrameworkCore;

    public class MarathonCoreDbContext : DbContext
    {
        public MarathonCoreDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<WorkItem> WorkItems => Set<WorkItem>();
        public DbSet<Project> Projects => Set<Project>();
        public DbSet<Story> Stories => Set<Story>();

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<WorkItem>(workItem =>
            {
                workItem.HasKey(wi => wi.WorkItemId);
                workItem.HasIndex(wi => wi.WorkItemId).IsUnique(true);
                workItem.HasIndex(wi => wi.Reference).IsUnique(true);
            });

            modelBuilder.Entity<Project>(project =>
            {
                project.HasKey(p => p.ProjectId);
                project.HasIndex(p => p.ProjectId).IsUnique(true);
                project.HasIndex(p => p.ProjectReference).IsUnique(true);
            });

            modelBuilder.Entity<Story>(story =>
            {
                story.HasKey(s => s.StoryId);
                story.HasIndex(s => new { s.ProjectId, s.WorkItemId }).IsUnique(true);
            });
        }
    }
}

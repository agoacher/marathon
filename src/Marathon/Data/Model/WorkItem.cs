﻿using System;

namespace Marathon.Data.Model
{
    public sealed class WorkItem :
        IEquatable<WorkItem>
    {
        private WorkItem()
        {
        }

        public WorkItem(string name, string description, Guid reference)
        {
            Name = name;
            Description = description;
            Reference = reference;
        }

        public int WorkItemId { get; private set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public Guid Reference { get; set; }

        public bool Equals(WorkItem other)
        {
            if(other == null)
            {
                return false;
            }

            return string.Equals(Name, other.Name)
                && string.Equals(Description, other.Description)
                && Reference ==  other.Reference
                && WorkItemId == other.WorkItemId;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Name, Description, Reference, WorkItemId);
        }

        public override bool Equals(object obj)
        {
            if(obj == null) { return false; }
            if(ReferenceEquals(this, obj)) { return true; }

            WorkItem otherStory = obj as WorkItem;
            if(otherStory == null) { return false; }

            return Equals(otherStory);
        }
    }
}

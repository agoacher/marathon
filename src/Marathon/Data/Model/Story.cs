﻿namespace Marathon.Data.Model
{
    public sealed class Story
    {
        public int StoryId { get; set; }

        public int WorkItemId { get; set; }

        public int ProjectId { get; set; }
    }
}

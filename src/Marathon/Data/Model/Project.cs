﻿namespace Marathon.Data.Model
{
    using System;

    public sealed class Project : IEquatable<Project>
    {
        private Project() { }

        public Project(string name, string description, Guid projectReference)
        {
            Name = name;
            Description = description;
            ProjectReference = projectReference;
        }

        public int ProjectId { get; private set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public Guid ProjectReference { get; set; }

        public bool Equals(Project other)
        {
            if (other == null)
            {
                return false;
            }

            return string.Equals(Name, other.Name)
                && string.Equals(Description, other.Description)
                && ProjectReference == other.ProjectReference
                && ProjectId == other.ProjectId;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) { return false; }
            if (ReferenceEquals(this, obj)) { return true; }

            Project otherProject = obj as Project;
            if (otherProject == null) { return false; }

            return Equals(otherProject);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Name, Description, ProjectReference, ProjectId);
        }
    }
}

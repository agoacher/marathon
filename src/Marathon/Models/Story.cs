﻿namespace Marathon.Models
{
    using System;

    public sealed class Story : IEquatable<Story>
    {
        public Story(string name, string description, Guid reference)
        {
            if(string.IsNullOrEmpty(name))
            {
                throw new ArgumentException(nameof(name));
            }

            if(string.IsNullOrEmpty(description))
            {
                throw new ArgumentException(nameof(description));
            }

            Name = name;
            Description = description;
            Reference = reference;
        }

        public string Name { get; }

        public string Description { get; }

        public Guid Reference { get; }

        public Guid? ProjectReference { get; set; }

        public bool Equals(Story other)
        {
            if(other == null) { return false; }

            return string.Equals(Name, other.Name)
                && string.Equals(Description, other.Description)
                && Reference == other.Reference
                && ProjectReference == other.ProjectReference;
        }

        public override bool Equals(object obj)
        {
           if(obj == null) { return false; }
           if(ReferenceEquals(this, obj)) { return true; }

            Story otherStory = obj as Story;
            if(otherStory == null) { return false; }

            return Equals(otherStory);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Name, Description, Reference, ProjectReference);
        }
    }
}

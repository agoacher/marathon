﻿namespace Marathon.Models
{
    using System;

    public sealed class Project : IEquatable<Project>
    {
        public Project(string name, string description, Guid reference)
        {
            if(string.IsNullOrEmpty(name))
            {
                throw new ArgumentException(nameof(name));
            }

            if(string.IsNullOrEmpty(description))
            {
                throw new ArgumentException(nameof(description));
            }

            Name = name;
            Description = description;
            Reference = reference;
        }

        public string Name { get; }

        public string Description { get; }

        public Guid Reference { get; }

        public override bool Equals(object obj)
        {
            if(obj == null) { return false; }

            if(ReferenceEquals(this, obj)) { return true; }

            Project other = obj as Project;
            if(other == null) { return false; }

            return Equals(other);
        }

        public bool Equals(Project other)
        {
            if(other == null) { return false; }

            return string.Equals(Name, other.Name)
                && string.Equals(Description, other.Description)
                && Reference == other.Reference;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Name, Description, Reference);
        }
    }
}

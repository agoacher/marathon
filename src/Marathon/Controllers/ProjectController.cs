﻿namespace Marathon.Controllers
{
    using Marathon.Models;
    using Marathon.Repository;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    [ApiController]
    [Route("/api/[controller]")]
    public class ProjectController : ControllerBase
    {
        private readonly ProjectRepository repository;

        public ProjectController(ProjectRepository repository)
        {
            this.repository = repository;
        }

        [HttpGet]
        public async Task<IEnumerable<Models.Project>> GetProjects(CancellationToken token)
        {
            var projects = await repository.GetAsync(token);

            return projects.Select(p => new Models.Project
            {
                Name = p.Name,
                Description = p.Description,
                Reference = p.Reference
            });
        }

        [HttpGet("/api/[controller]/info")]
        public async Task<IEnumerable<Models.ProjectInfo>> GetProjectInfo(CancellationToken token)
        {
            var projects = await repository.GetAsync(token);
            return projects.Select(p => new Models.ProjectInfo
            {
                Name = p.Name,
                Reference = p.Reference
            });
        }

        [HttpPost("/api/[controller]/new")]
        public async Task<Guid> AddProject([FromBody] Models.Project project, CancellationToken token)
        {
            if (project == null)
            {
                throw new NullReferenceException(nameof(project));
            }

            var newId = Guid.NewGuid();
            var newproject = new Project(project.Name, project.Description, newId);

            await repository.SaveAsync(newproject, token);
            return newId;
        }

        [HttpPost("/api/[controller]/update")]
        public async Task UpdateProject([FromBody] Models.Project project, CancellationToken token)
        {
            if (project == null)
            {
                throw new NullReferenceException(nameof(project));
            }

            var updatedproject = new Project(project.Name, project.Description, project.Reference.Value);

            await repository.UpdateAsync(updatedproject, token);
        }

        [HttpDelete]
        public async Task DeleteProject([FromQuery] Guid @ref, CancellationToken token)
        {
            await repository.DeleteByReferenceAsync(@ref, token);
        }
    }
}

﻿namespace Marathon.Controllers
{
    using Marathon.Models;
    using Marathon.Repository;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    [Route("/api/[controller]")]
    [ApiController]
    public class StoryController : ControllerBase
    {
        private readonly StoryRepository repository;

        public StoryController(StoryRepository repository)
        {
            this.repository = repository;

        }

        [HttpGet]
        public async Task<IEnumerable<Models.Story>> GetStories(CancellationToken cancellationToken)
        {
            var stories = await repository.GetAsync(cancellationToken);

            return stories.Select(s => new Models.Story
            {
                Name = s.Name,
                Description = s.Description,
                Reference = s.Reference,
                ProjectReference = s.ProjectReference
            });
        }

        [HttpPost("/api/[controller]/new")]
        public async Task<Guid> AddStory([FromBody] Models.Story story, CancellationToken token)
        {
            if (story == null)
            {
                throw new NullReferenceException(nameof(story));
            }

            var id = Guid.NewGuid();
            var newStory = new Story(story.Name, story.Description, id);

            await repository.SaveAsync(newStory, token);
            if (story.ProjectReference.HasValue)
            {
                await repository.AssignProjectToStoryAsync(story.ProjectReference.Value, id, token);
            }
            return id;
        }

        [HttpPost("/api/[controller]/update")]
        public async Task UpdateStory([FromBody] Models.Story story, CancellationToken token)
        {
            if (story == null)
            {
                throw new NullReferenceException(nameof(story));
            }

            var updatedStory = new Story(story.Name, story.Description, story.Reference.Value);

            await repository.UpdateAsync(updatedStory, token);

            if (story.ProjectReference.HasValue)
            {
                await repository.AssignProjectToStoryAsync(story.ProjectReference.Value, story.Reference.Value, token);
            }
            else
            {
                await repository.DeleteProjectReference(story.Reference.Value, token);
            }
        }

        [HttpDelete]
        public async Task DeleteStory([FromQuery] Guid @ref, CancellationToken token)
        {
            await repository.DeleteByReferenceAsync(@ref, token);
        }
    }
}

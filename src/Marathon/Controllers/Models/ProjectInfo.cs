﻿namespace Marathon.Controllers.Models
{
    using System;

    public sealed class ProjectInfo
    {
        public string Name { get; set; }

        public Guid Reference { get; set; }
    }
}

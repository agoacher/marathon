﻿namespace Marathon.Controllers.Models
{
    using System;

    public sealed class Project
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public Guid? Reference { get; set; }
    }
}

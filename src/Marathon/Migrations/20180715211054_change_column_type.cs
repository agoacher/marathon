﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Marathon.Migrations
{
    public partial class change_column_type : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<Guid>(
                name: "Reference",
                table: "WorkItems",
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 20);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Reference",
                table: "WorkItems",
                maxLength: 20,
                nullable: false,
                oldClrType: typeof(Guid));
        }
    }
}

﻿import React from 'react';
import '../css/modal.css';

const Modal = ({ onClose, title, children, show }) => {
    if (!show) { return null; }

    return (
        <div className="modal-container active">
            <label className="modal-backdrop"></label>
            <div className="modal-content">
                <label className="modal-close" onClick={(e) => onClose()}>&#x2715;</label>
                <h2>{title}</h2>
                <hr />
                {children}
            </div >
        </div >
    );
};

Modal.displayName = "Modal";
export default Modal;
﻿import React from 'react';
import { NavLink } from 'react-router-dom';
import '../css/NavMenu.css';

const NavMenu = () => {
    return (
        <nav>
            <ul>
                <li><NavLink activeClassName="active" id="logo" exact to={'/'}>Marathon</NavLink></li>
                <li><NavLink activeClassName="active" to={'/projects'}>Projects</NavLink></li>
                <li><NavLink activeClassName="active" to={'/stories'}>Stories</NavLink></li>
            </ul>
        </nav>);
};

NavMenu.displayName = "NavMenu";
export default NavMenu;
﻿import React from 'react';

const SelectEx = ({ name, value, options, onChange }) => {
    const selectItems = options.map((opt, index) => {
        return <option key={index} value={opt.value}>{opt.name}</option>;
    });

    return (
        <select name={name} value={value} onChange={onChange}>
            {selectItems}
        </select>
    );
};

SelectEx.displayName = "SelectEx";

const createOption = (name, value) => {
    return { name: name, value: value };
};

export default SelectEx;
export { createOption };
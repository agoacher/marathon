﻿import React from 'react';
import '../css/collapsible.css';

const CollapsibleBody = ({ collapsed, children }) => {
    const style = {
        display: collapsed ? "none" : "block"
    };

    return (
        <div className="content" style={style}>
            {children}
        </div>
    );
};

CollapsibleBody.displayName = "CollapsibleBody";

export default CollapsibleBody;
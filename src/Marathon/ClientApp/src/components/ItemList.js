﻿import React from 'react';
import '../css/list_style.css';

const ItemList = ({ items, header, footer, headerClicked, footerClicked,className }) => {
    const css = `lists ${className}`;
    const listItems = items.map((item, index) => {
        return <li key={index}>{item}</li>;
    });

    return (
        <div className={css}>
            <div className="list">
                <header onClick={(e) => headerClicked(e)}>{header}</header>
                <ul>
                    {listItems}
                </ul>
                <footer onClick={(e) => footerClicked(e)}>{footer}</footer>
            </div>
        </div>
    );
};

ItemList.displayName = "ItemList";
export default ItemList;
﻿import React from 'react';
import '../css/collapsible.css';

const CollapsibleHeader = ({ collapsed, children, onCollapsedChanged }) => {
    const activeStyle = collapsed ? "" : " active";
    const css = `collapsible${activeStyle}`;

    return (
        <div className={css} onClick={(e) => onCollapsedChanged()}>
            {children}
        </div>
    );
};

CollapsibleHeader.displayName = "CollapsibleHeader";

export default CollapsibleHeader;
﻿import { get, post, del, hashcode, colorFromHash } from './Api';

const projectApiRoute = "/api/project";

const getProjectData = () => {
    const promise = new Promise((resolve, reject) => {
        get(projectApiRoute).then(response => {
            response.json().then(data => {
                const projectData = data.map(d => {
                    const color = colorFromHash(hashcode(d.name));
                    return {
                        project: d,
                        color: color
                    };
                });
                resolve(projectData);
            }, error => {
                reject(error);
            });
        }, error => {
            reject(error);
        });
    });

    return promise;
};

const getProjectInfo = () => {
    const promise = new Promise((resolve, reject) => {
        const url = `${projectApiRoute}/info`;
        get(url).then(response => {
            response.json().then(data => {
                resolve(data);
            }, error => {
                reject("error getting project info");
            });
        }, error => {
            reject(error);
        });
    });

    return promise;
};

const saveProject = (project) => {
    const promise = new Promise((resolve, reject) => {
        const url = `${projectApiRoute}/new`;
        post(url, project).then(response => {
            response.json().then(data => {
                resolve(data);
            }, error => reject(error));
        }, error => {
            reject(error);
        });
    });

    return promise;
};

const updateProject = (project) => {
    const url = `${projectApiRoute}/update`;
    return post(url, project);
};

const deleteProject = (project) => {
    const promise = new Promise((resolve, reject) => {
        const url = `${projectApiRoute}?ref=${project.reference}`;

        del(url).then(response => {
            if (!response.ok) {
                reject("delete not successful");
            } else {
                resolve();
            }
        }, error => {
            reject(error);
        });
    });
    return promise;
};

export { getProjectData, saveProject, updateProject, deleteProject, getProjectInfo };
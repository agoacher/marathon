﻿const post = (url, content) => {
    return fetch(url, {
        method: "POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        },
        body: JSON.stringify(content)
    });
};

const get = (url) => {
    return fetch(url, {
        method: "GET",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        }
    });
};

const del = (url) => {
    return fetch(url, {
        method: "DELETE",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        }
    });
};

// I think this belongs client side, it's a purely ui feature so I lifted this
// hashcode functionality from:
// https://gist.github.com/hyamamoto/fd435505d29ebfa3d9716fd2be8d42f0
const hashcode = (str) => {
    let h = 0;
    const l = str.length;
    let i = 0;

    if (l > 0) {
        while (i < l) {
            h = (h << 5) - h + str.charCodeAt(i++) | 0;
        }
    }
    return h;
};

const colorFromHash = (hashcode) => {
    const r = (hashcode >> 24) & 0xFF;
    const g = (hashcode >> 16) & 0xFF;
    const b = (hashcode >> 8) & 0xFF;
    const a = hashcode & 0xFF;

    return {
        r: r, g: g, b: b, a: a
    };
};

export { post, del, get, hashcode, colorFromHash };
﻿import { get, post, del } from './Api';

const storyApiRoute = "/api/story";

const getStories = () => {
    const promise = new Promise((resolve, reject) => {
        get(storyApiRoute)
            .then(response => {
                response.json().then(data => {
                    resolve(data);
                });
            },
            error => {
                reject(error);
            });
    });

    return promise;
};

const saveStory = (story) => {
    const promise = new Promise((resolve, reject) => {
        const url = `${storyApiRoute}/new`;
        post(url, story).then(response => {
            response.json().then(data => {
                resolve(data);
            }, error => reject(error));
        }, error => {
            reject(error);
        });
    });

    return promise;
};

const updateStory = (story) => {
    const url = `${storyApiRoute}/update`;
    return post(url, story);
};

const deleteStory = (story) => {
    const url = `${storyApiRoute}?ref=${story.reference}`;
    const promise = new Promise((resolve, reject) => {
        del(url).then(response => {
            if (!response.ok) {
                reject("unsuccessful delete");
            } else {
                resolve();
            }

        }, error => {
            reject(error);
        });
    });
    return promise;
};

export { getStories, saveStory, updateStory, deleteStory };
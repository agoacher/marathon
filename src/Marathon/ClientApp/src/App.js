import React, { Component } from 'react';
import { Route } from 'react-router';
import Layout from './pages/Layout';

import { HomePage } from './pages/HomePage';
import { StoriesPage } from './pages/StoriesPage';
import { ProjectsPage } from './pages/ProjectsPage';

export default class App extends Component {
  displayName = App.name

  render() {
    return (
      <Layout>
            <Route exact path='/' component={HomePage} />
            <Route exact path='/projects' component={ProjectsPage} />
            <Route exact path='/stories' component={StoriesPage} />
      </Layout>
    );
  }
}

﻿import React, { Component } from 'react';

import Modal from '../components/Modal';
import Story from '../models/Story';
import ItemList from '../components/ItemList';
import CollapsibleHeader from '../components/collapsibleHeader';
import CollapsibleBody from '../components/collapsibleBody';
import SelectEx, { createOption } from '../components/extended-select';

import { getStories, saveStory, updateStory, deleteStory } from '../services/storyService';
import { getProjectInfo } from '../services/projectService';
import '../css/index.css';

export class StoriesPage extends Component {
    displayName = StoriesPage.name

    constructor(props) {
        super(props);

        const storyState = {
            stories: [],
            filteredStories: [],
            selectedFilter: "all",
            selectedStory: ""
        };

        const projectState = {
            projects: []
        };

        this.state = {
            modalState: this.defaultModalState(),
            storyState: storyState,
            projectState: projectState
        };

        this.onCollapsedChanged = this.onCollapsedChanged.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.addStoryModalClosed = this.addStoryModalClosed.bind(this);
        this.showModal = this.showModal.bind(this);
        this.deleteStory = this.deleteStory.bind(this);
    }

    componentWillMount() {
        Promise.all([getStories(), getProjectInfo()]).then(data => {
            const storyState = this.getStatePart(this.state.storyState, { stories: data[0], filteredStories: data[0] });
            const projectState = this.getStatePart(this.state.projectState, { projects: data[1] });
            this.setState({
                storyState: storyState,
                projectState: projectState
            });
        }, error => console.error(error));
    }

    getStatePart(initial, parts) {
        return (initial === null) ? parts : Object.assign(initial, parts);
    }

    defaultModalState() {
        return {
            visible: false,
            editing: false,
            name: "",
            description: "",
            reference: "",
            projectReference: ""
        };
    }

    addStoryModalClosed() {
        this.setState({
            modalState: this.defaultModalState()
        });
    }

    showModal(story) {
        let modalState = (story === null) ? this.defaultModalState() :
            {
                visible: true,
                editing: true,
                name: story.name,
                description: story.description,
                reference: story.reference,
                projectReference: story.project || ""
            };
        modalState.visible = true;
        this.setState({
            modalState: modalState
        });
    }

    deleteStory(story) {
        deleteStory(story).then(r => {
            const storyState = this.state.storyState;
            const stories = storyState.stories.filter(s => {
                return s.reference !== story.reference;
            });
            this.setState({
                storyState: this.getStatePart(storyState, { stories: stories, filteredStories: this.filterStories(stories, storyState.selectedFilter), selectedStory: "" })
            });
        });
    }

    updateStories() {
        const promise = new Promise((resolve, reject) => {
            const modalState = this.state.modalState;
            const storyState = this.state.storyState;
            const isEdit = modalState.editing;
            let stories = storyState.stories;
            let story = null;
            if (isEdit) {
                story = stories.find(s => s.reference === modalState.reference).story;
                story.name = modalState.name;
                story.description = modalState.description;
                story.projectReference = modalState.project;
                updateStory(story).then(s => resolve(stories));
            } else {
                story = new Story(modalState.name, modalState.description, modalState.reference, modalState.project);
                saveStory(story).then(id => {
                    story.reference = id;
                    stories = stories.concat([story]);
                    this.setState({ storyState: this.getStatePart(storyState, { selectedStory: story.reference }) });
                    resolve(stories);
                });
            }
        });
        return promise;
    }

    filterStories(stories, filter) {
        let filteredStories = null;
        switch (filter) {
            case "all": return stories;
            case "none": return stories.filter(s => s.projectReference === "" || s.projectReference === null);
            default: return stories.filter(s => s.projectReference === filter);
        }
    }

    handleInputChange(e) {
        const target = e.target;
        const name = target.name;
        const value = target.value;

        let modalState = this.state.modalState;
        let storyState = this.state.storyState;
        let projectState = this.state.projectState;

        switch (name) {
            case "modalFilter":
                modalState = this.getStatePart(modalState, { project: value });
                break;
            case "projectFilter":
                storyState = this.getStatePart(storyState, { filteredStories: this.filterStories(storyState.stories, value), selectedFilter: value });
                break;
            case "addStoryName":
                modalState = this.getStatePart(modalState, { name: value });
                break;
            case "addStoryDescription":
                modalState = this.getStatePart(modalState, { description: value });
                break;
            default:
                console.error(`${name} is not a valid option`);
                break;
        }
        this.setState({
            modalState: modalState,
            storyState: storyState,
            projectState: projectState
        });
    }

    handleFormSubmit(e) {
        e.preventDefault();

        this.updateStories().then(stories => {
            this.setState({
                modalState: this.defaultModalState(),
                storyState: this.getStatePart(this.state.storyState, { stories: stories, filteredStories: this.filterStories(stories, this.state.storyState.selectedFilter) })
            });
        }, error => console.error(error));
    }

    onCollapsedChanged(reference) {
        let newReference = "";
        let storyState = this.state.storyState;

        if (reference !== storyState.selectedStory) {
            newReference = reference;
        }

        this.setState({
            storyState: this.getStatePart(storyState, { selectedStory: newReference })
        });
    }

    render() {
        const items = this.mapStoriesIntoItemList();
        const defaultOptions = [createOption("Choose a project...", "")];
        const allOptions = defaultOptions.concat(this.state.projectState.projects.map(p => {
            return createOption(p.name, p.reference);
        }));

        const modal = this.state.modalState;

        return (
            <div>
                <ItemList
                    className="center"
                    header="Stories"
                    items={items}
                    footer="Add a story..."
                    footerClicked={(e) => this.showModal(null)} />

                <Modal
                    title="Add Story"
                    show={modal.visible}
                    onClose={this.addStoryModalClosed}>
                    <form onSubmit={this.handleFormSubmit} name="modalForm" className="marathon-modal">
                        <SelectEx name="modalFilter" value={modal.projectReference} options={allOptions} onChange={this.handleInputChange} />
                        <input name="addStoryName" value={modal.name} onChange={this.handleInputChange} type="text" placeholder="Name..." />
                        <textarea name="addStoryDescription" value={modal.description} onChange={this.handleInputChange} placeholder="Description..." />
                        <input type="submit" value="Submit" />
                        <button onClick={this.addStoryModalClosed}> Cancel</button>
                    </form>
                </Modal>
            </div>
        );
    }

    mapStoriesIntoItemList() {
        const storyState = this.state.storyState;
        const items = storyState.filteredStories.map(story => {
            const collapsed = story.reference !== this.state.storyState.selectedStory;
            return (
                <div>
                    <CollapsibleHeader
                        collapsed={collapsed}
                        onCollapsedChanged={() => this.onCollapsedChanged(story.reference)}>
                        <span>{story.name}</span>
                    </CollapsibleHeader>
                    <CollapsibleBody
                        collapsed={collapsed}>
                        <p>{story.description}</p>
                        <p>
                            <button onClick={(e) => this.showModal(story)}>edit</button>
                            <button onClick={(e) => this.deleteStory(story)}>delete</button>
                        </p>
                    </CollapsibleBody>
                </div>
            );
        });

        const defaultOptions = [
            createOption("All", "all"),
            createOption("No Project", "none")];
        const allOptions = defaultOptions.concat(this.state.projectState.projects.map(p => {
            return createOption(p.name, p.reference);
        }));

        return [<SelectEx name="projectFilter"
            value={this.state.storyState.selectedFilter}
            options={allOptions}
            onChange={this.handleInputChange} />].concat(items);
    }
}
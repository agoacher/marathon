﻿import React, { Component } from 'react';

import Modal from '../components/Modal';
import Project from '../models/Project';
import ItemList from '../components/ItemList';
import CollapsibleBody from '../components/collapsibleBody';
import CollapsibleHeader from '../components/collapsibleHeader';

import { getProjectData, updateProject, saveProject, deleteProject } from '../services/projectService';
import '../css/index.css';

export class ProjectsPage extends Component {
    displayName = ProjectsPage.name

    constructor(props) {
        super(props);

        this.state = {
            modal: {
                visible: false,
                name: "",
                description: "",
                reference:""
            },
            projects: [],
            selectedProject: ""
        };

        this.addProjectClicked = this.addProjectClicked.bind(this);
        this.addprojectModalClosed = this.addprojectModalClosed.bind(this);
        this.handleModalInputChange = this.handleModalInputChange.bind(this);

        this.editproject = this.editproject.bind(this);
        this.deleteProject = this.deleteProject.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
    }

    componentWillMount() {
        getProjectData().then(data => {
            this.setState({ projects: data.map(p => p.project)});
        }, error => {
            console.error(error);
        });
    }

    handleFormSubmit(e) {
        e.preventDefault();

        let projects = this.state.projects;
        const modal = this.state.modal;

        if (modal.editing) {
            let project = projects.find(s => s.reference === modal.reference);
            project.name = modal.name;
            project.description = modal.description;
            updateProject(project);
        } else {
            let project = new Project(modal.name, modal.description, "");
            saveProject(project).then(id => {
                project.reference = id;
                projects = projects.concat([project]);
                let newModalState = Object.assign(modal, {
                    visible: false,
                    name: "",
                    description: ""
                });

                this.setState({
                    modal: newModalState,
                    projects: projects
                });
            }, error => console.error(error));
        }
    }

    addprojectModalClosed() {
        const modal = this.state.modal;

        let newModalState = Object.assign(modal, {
            visible: false,
            name: "",
            description: "",
            reference: "",
            editing: false
        });

        this.setState({
            modal: newModalState
        });
    }

    addProjectClicked(e) {
        let newModalState = Object.assign(this.state.modal, { visible: true, editing: false });
        this.setState({
            modal: newModalState
        });
    }

    handleModalInputChange(e) {
        const target = e.target;
        const value = target.value;
        const name = target.name;

        let modal = this.state.modal;
        let part = {};

        switch (name) {
            case "addprojectName":
                part = { name: value };
                break;
            case "addprojectDescription":
                part = { description: value };
                break;
            default:
                console.error(`${name} is not a valid input option`);
                break;
        }

        modal = Object.assign(modal, part);
        this.setState({
            modal: modal
        });
    }

    editproject(project) {
        const modalState = {
            visible: true,
            name: project.name,
            description: project.description,
            editing: true,
            reference: project.reference
        };

        this.setState({
            modal: modalState
        });
    }

    deleteProject(project) {
        deleteProject(project).then(success => {
            let projects = this.state.projects.filter(s => {
                return s.reference !== project.reference;
            });
            this.setState({
                projects: projects
            });
        }, error => console.error(error));
    }

    onCollapsedChanged(reference) {
        if (reference === this.state.selectedProject) { reference = ""; }
        this.setState({
            selectedProject: reference
        });
    }

    mapprojects() {
        return this.state.projects.map(project => {
            const isCollapsed = project.reference !== this.state.selectedProject;
            return (
                <div>
                    <CollapsibleHeader
                        collapsed={isCollapsed}
                        onCollapsedChanged={() => this.onCollapsedChanged(project.reference)}
                    >
                        <span>{project.name}</span>
                    </CollapsibleHeader>
                    <CollapsibleBody
                        collapsed={isCollapsed}
                    >
                        <p>{project.description}</p>
                        <p><button onClick={(e) => this.editproject(project)}>edit</button></p>
                        <p><button onClick={(e) => this.deleteProject(project)}>delete</button></p>
                    </CollapsibleBody>
                </div>
            );
        });
    }

    render() {
        const items = this.mapprojects();

        return (
            <div>
                <ItemList
                    className="center"
                    header="projects"
                    items={items}
                    footer="Add a project..."
                    footerClicked={this.addProjectClicked}
                />

                <Modal
                    title="Add project"
                    show={this.state.modal.visible}
                    onClose={this.addprojectModalClosed}
                >
                    <form onSubmit={this.handleFormSubmit} name="modalForm" className="marathon-modal">
                        <input name="addprojectName" value={this.state.modal.name} onChange={this.handleModalInputChange} type="text" placeholder="Name..." />

                        <textarea name="addprojectDescription" value={this.state.modal.description} onChange={this.handleModalInputChange} placeholder="Description..." />

                        <input type="submit" value="Submit" />
                        <button onClick={this.addprojectModalClosed}> Cancel</button>
                    </form>
                </Modal>
            </div>
        );
    }
}
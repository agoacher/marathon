import React from 'react';
import NavMenu from '../components/NavMenu';

const Layout = ({ children }) => {
    return (
        <div>
            <NavMenu />
            <br />
            <div>{children}</div>
        </div>
    );
};

Layout.displayName = "Layout";
export default Layout;

import React, { Component } from 'react';

export class HomePage extends Component {
    displayName = HomePage.name;

    render() {
        return (
            <div>
                <h1>Watch this space!</h1>

                <p>
                    Something awesome, this way comes.
                </p>
            </div>
        );
    }
}

﻿const Story = (name, description, reference, projectReference) => {
    return {
        name: name,
        description: description,
        reference: reference,
        projectReference: projectReference
    };
};

export default Story;
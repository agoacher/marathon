﻿const Project = (name, description, reference) => {
    return {
        name: name,
        description: description,
        reference: reference
    };
};

export default Project;
﻿namespace Marathon.Repository
{
    using Marathon.Data;
    using Marathon.Models;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    public class StoryRepository : IAsyncWriter<Story>, IAsyncReader<Story>
    {
        private readonly MarathonCoreDbContext context;

        public StoryRepository(MarathonCoreDbContext context)
        {
            this.context = context;
        }

        public async Task DeleteByReferenceAsync(Guid reference, CancellationToken token)
        {
            var workItem = await context.WorkItems.SingleAsync(x => x.Reference == reference, token);
            await DeleteProjectReference(reference, token);
            context.Remove(workItem);
            await context.SaveChangesAsync(token);
        }

        public async Task<IEnumerable<Story>> GetAsync(CancellationToken cancellationToken)
        {
            var stories = await (from wi in context.WorkItems
                                 join s in context.Stories on wi.WorkItemId equals s.WorkItemId into joinedS
                                 from s in joinedS.DefaultIfEmpty()
                                 join p in context.Projects on s.ProjectId equals p.ProjectId into joinedP
                                 from p in joinedP.DefaultIfEmpty()
                                 select new Story(wi.Name, wi.Description, wi.Reference) { ProjectReference = p.ProjectReference }).ToListAsync(cancellationToken);

            return stories;
        }

        public async Task DeleteProjectReference(Guid storyReference, CancellationToken token)
        {
            var workItem = await context.WorkItems.SingleAsync(x => x.Reference == storyReference, token);
            var story = await context.Stories.SingleOrDefaultAsync(x => x.StoryId == workItem.WorkItemId, token);
            if (story != null)
            {
                context.Remove(story);
                await context.SaveChangesAsync(token);
            }
        }

        public async Task SaveAsync(Story item, CancellationToken token)
        {
            var dataModel = new Data.Model.WorkItem(item.Name, item.Description, item.Reference);
            context.WorkItems.Add(dataModel);

            await context.SaveChangesAsync(token);
        }

        public async Task UpdateAsync(Story item, CancellationToken token)
        {
            var workItem = await context.WorkItems.SingleAsync(x => x.Reference == item.Reference, token);
            workItem.Name = item.Name;
            workItem.Description = item.Description;

            context.Update(workItem);
            await context.SaveChangesAsync(token);
        }

        public async Task AssignProjectToStoryAsync(Guid project, Guid story, CancellationToken token)
        {
            var workItem = await context.WorkItems.SingleAsync(x => x.Reference == story, token);
            var projectItem = await context.Projects.SingleAsync(x => x.ProjectReference == project, token);

            var existingLink = await context.Stories.SingleOrDefaultAsync(x => x.WorkItemId == workItem.WorkItemId, token);
            if (existingLink != null)
            {
                context.Remove(existingLink);
            }

            var storyLink = new Data.Model.Story
            {
                ProjectId = projectItem.ProjectId,
                WorkItemId = workItem.WorkItemId
            };
            context.Add(storyLink);

            await context.SaveChangesAsync(token);
        }
    }
}

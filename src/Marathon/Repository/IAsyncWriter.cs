﻿namespace Marathon.Repository
{
    using System.Threading;
    using System.Threading.Tasks;

    public interface IAsyncWriter<T>
    {
        Task SaveAsync(T item, CancellationToken token);

        Task UpdateAsync(T item, CancellationToken token);
    }
}

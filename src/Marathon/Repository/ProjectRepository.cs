﻿namespace Marathon.Repository
{
    using Marathon.Data;
    using Marathon.Models;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    public class ProjectRepository :
        IAsyncWriter<Project>,
        IAsyncReader<Project>
    {
        private readonly MarathonCoreDbContext context;

        public ProjectRepository(MarathonCoreDbContext context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<Project>> GetAsync(CancellationToken cancellationToken)
        {
            var projects = await context.Projects.ToListAsync(cancellationToken);
            return projects.Select(p => new Project(p.Name, p.Description, p.ProjectReference));
        }

        public async Task SaveAsync(Project item, CancellationToken token)
        {
            var dataModel = new Data.Model.Project(item.Name, item.Description, item.Reference);
            context.Projects.Add(dataModel);

            await context.SaveChangesAsync(token);
        }

        public async Task UpdateAsync(Project item, CancellationToken token)
        {
            var project = await context.Projects.SingleAsync(p => p.ProjectReference == item.Reference, token);
            project.Name = item.Name;
            project.Description = item.Description;

            context.Update(project);
            await context.SaveChangesAsync(token);
        }

        public async Task DeleteByReferenceAsync(Guid reference, CancellationToken token)
        {
            var project = await context.Projects.SingleAsync(x => x.ProjectReference == reference, token);
            context.Remove(project);
            await context.SaveChangesAsync(token);
        }
    }
}

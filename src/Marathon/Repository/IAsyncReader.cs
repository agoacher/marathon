﻿namespace Marathon.Repository
{
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;

    public interface IAsyncReader<T>
    {
        Task<IEnumerable<T>> GetAsync(CancellationToken cancellationToken);
    }
}
